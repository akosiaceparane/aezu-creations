import Vue from 'vue';
import Styles from './scss/app.module';
import Parallax from 'parallax-js';
import $ from 'jquery';

const requireComponent = require.context(
    // The relative path of the components folder
    './components',
    // Whether or not to look in subfolders
    false,
    // The regular expression used to match base component filenames
    /[A-Z]\w+\.(vue|js)$/
  )
  
  requireComponent.keys().forEach(fileName => {
    // Get component config
    const componentConfig = requireComponent(fileName)
  
    // Get PascalCase name of component
    const componentName = fileName
          .split('/')
          .pop()
          .replace(/\.\w+$/, '')
          .replace(/([a-z0-9])([A-Z])/g, '$1-$2')
          .toLowerCase();
  
    // Register component globally
    Vue.component(
      componentName,
      // Look for the component options on `.default`, which will
      // exist if the component was exported with `export default`,
      // otherwise fall back to module's root.
      componentConfig.default || componentConfig
    );
  })

  new Vue({
    el : "#parallax-header",
    data : {
      classes : [Styles.parallaxHeader]
    },
    mounted() {

    }
  })